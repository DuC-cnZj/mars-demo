package main

import (
	"log"
	"net"
	"net/http"
	"strings"
	"time"
)

func main() {
	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		host, _, _ := net.SplitHostPort(r.RemoteAddr)
		log.Println("visit '/' " + time.Now().Format("2006-01-02 15:04:05") + " ip:" + host + " clientIP: " + ClientIP(r))
		rw.Write([]byte("demo v2"))
	})

	http.HandleFunc("/ping", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		rw.WriteHeader(200)
		rw.Write([]byte("{\"data\":\"pong\"}"))
	})

	log.Println("run at :8080")
	http.ListenAndServe(":8080", nil)
}

func ClientIP(r *http.Request) string {
	xForwardedFor := r.Header.Get("X-Forwarded-For")
	ip := strings.TrimSpace(strings.Split(xForwardedFor, ",")[0])
	if ip != "" {
		return ip
	}

	ip = strings.TrimSpace(r.Header.Get("X-Real-Ip"))
	if ip != "" {
		return ip
	}

	if ip, _, err := net.SplitHostPort(strings.TrimSpace(r.RemoteAddr)); err == nil {
		return ip
	}

	return ""
}
